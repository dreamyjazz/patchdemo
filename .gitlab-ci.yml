variables:
  IMAGE_TAG:
    description: "commit sha to be used as image tag, or latest"
    value: "latest"
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_FORCE_HTTPS: "true"

include:
  - project: repos/releng/kokkuri
    file: includes/images.yaml

stages:
  - test
  - publish
  - deploy

test-php:
  stage: test
  image:
    name: docker-registry.wikimedia.org/releng/composer-test-php82:0.1.2-s2
    entrypoint: [ "" ]
  allow_failure: false
  script:
    - composer install --prefer-dist --no-progress --no-suggest
    - composer run-script test

test-nodejs:
  stage: test
  image:
    name: docker-registry.wikimedia.org/releng/node16-test:0.2.1
    entrypoint: [ "" ]
  allow_failure: false
  script:
    - npm ci
    - npm run build --if-present
    - npm test

.protected_rules:
  rules:
    - if: $CI_PROJECT_NAMESPACE == "repos/test-platform/catalyst" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
  manual_rules:
    - if: $CI_PROJECT_NAMESPACE == "repos/test-platform/catalyst" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_REF_PROTECTED
      when: manual

publish:
  stage: publish
  variables:
    BUILD_VARIANT: patchdemo
    PUBLISH_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
    PUBLISH_IMAGE_EXTRA_TAGS: latest
  extends: .kokkuri:build-and-publish-image
  tags:
    - trusted
  rules:
    - !reference [.protected_rules, rules]

deploy-staging:
  stage: deploy
  image:
    name: docker-registry.wikimedia.org/repos/abstract-wiki/ci-images/helm
    entrypoint: [ "" ]
  variables:
    IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
  script:
    - kubectl config use-context repos/test-platform/catalyst/catalyst-api:api-deploy-agent
    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - |
      helm upgrade --install --dependency-update --namespace patchdemo-staging --create-namespace \
        patchdemo-staging ./helm/patchdemo \
        --set image.tag=${IMAGE_TAG} \
        --set service.type=ClusterIP \
        --set ingress.enabled=true \
        --set ingress.hostname="staging-patchdemo.wmcloud.org" \
        --set mariadb.auth.username=${DB_USER} \
        --set mariadb.auth.password=${DB_PASS} \
        --set mariadb.auth.rootPassword=${DB_ROOT_PASS} \
        --set mariadb.auth.database=${DB_DATABASE} \
        --set app.db.host=${DB_HOST} \
        --set app.catalystApiUrl="https://staging-api.catalyst.wmcloud.org" \
        --set app.catalystApiToken=${CATALYST_API_TOKEN} \
        --set app.oauthCallback="https://staging-patchdemo.wmcloud.org/" \
        --set app.oauthConsumerKey=${OAUTH_CONSUMER_KEY} \
        --set app.oauthConsumerSecret=${OAUTH_CONSUMER_SECRET} \
        --set app.conduitApiKey=${CONDUIT_API_KEY} \
        --set app.notificationBanner="${NOTIFICATION_BANNER}" \
        --set wikiReposPool.hostPath=${REPO_POOL_HOST_PATH}
  tags: [ trusted ]
  environment:
    name: patchdemo-staging
  rules: 
    - !reference [.protected_rules, rules]

deploy-production:
  stage: deploy
  image:
    name: docker-registry.wikimedia.org/repos/abstract-wiki/ci-images/helm
    entrypoint: [ "" ]
  variables:
    IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
  script:
    - kubectl config use-context repos/test-platform/catalyst/catalyst-api:api-deploy-agent
    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - |
      helm upgrade --install --dependency-update --namespace patchdemo --create-namespace \
        patchdemo ./helm/patchdemo \
        --set image.tag=${IMAGE_TAG} \
        --set service.type=ClusterIP \
        --set ingress.enabled=true \
        --set ingress.hostname="patchdemo.wmcloud.org" \
        --set mariadb.auth.username=${DB_USER} \
        --set mariadb.auth.password=${DB_PASS} \
        --set mariadb.auth.rootPassword=${DB_ROOT_PASS} \
        --set mariadb.auth.database=${DB_DATABASE} \
        --set app.db.host=${DB_HOST} \
        --set app.catalystApiUrl="https://api.catalyst.wmcloud.org" \
        --set app.catalystApiToken=${CATALYST_API_TOKEN} \
        --set app.oauthCallback="https://patchdemo.wmcloud.org/" \
        --set app.oauthConsumerKey=${OAUTH_CONSUMER_KEY} \
        --set app.oauthConsumerSecret=${OAUTH_CONSUMER_SECRET} \
        --set app.conduitApiKey=${CONDUIT_API_KEY} \
        --set app.notificationBanner="${NOTIFICATION_BANNER}" \
        --set wikiReposPool.hostPath=${REPO_POOL_HOST_PATH}
  tags: [ trusted ]
  environment:
    name: patchdemo-production
  rules:
    - !reference [.protected_rules, manual_rules]
